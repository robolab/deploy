stages:
  - build
  - release

variables:
  PACKAGE_VERSION: ${CI_COMMIT_TAG}
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/singularity/${PACKAGE_VERSION}"

build_amd64:
  stage: build
  image:
    name: quay.io/singularity/singularity:v4.1.0
    entrypoint: ["/bin/sh", "-c"]
  rules:
    - if: $CI_COMMIT_TAG
  cache:
    key: singularity-cache
    paths:
      - /root/.singularity/cache
  script:
    - |
      apk add --no-cache bash curl
      echo -e "machine gitlab.fel.cvut.cz\nlogin gitlab-ci-token\npassword ${ARO_STUDENT_REPO_ACCESS_TOKEN}" >> /root/.netrc
      PATH=`pwd`/singularity:$PATH /bin/bash singularity/build_images
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file singularity/robolab_noetic.simg ${PACKAGE_REGISTRY_URL}/robolab_noetic_amd64.simg
    - |
      sha256sum singularity/robolab_noetic.simg | cut -d' ' -f1 > singularity/robolab_noetic.simg.sha256sum
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file singularity/robolab_noetic.simg.sha256sum ${PACKAGE_REGISTRY_URL}/robolab_noetic_amd64.simg.sha256sum

build_arm64:
  stage: build
  image:
    name: quay.io/singularity/singularity:v4.1.0
    entrypoint: ["/bin/sh", "-c"]
  rules:
    - if: $CI_COMMIT_TAG
  cache:
    key: singularity-cache
    paths:
      - /root/.singularity/cache
  script:
    # This build uses some qemu + binfmt magic to emulate the arm64 arch on amd64 runner. This build is pretty slow, though. 
    # https://github.com/qemu/qemu/blob/master/scripts/qemu-binfmt-conf.sh
    # https://github.com/multiarch/qemu-user-static/blob/master/containers/latest/register.sh
    - |
      apk add --no-cache bash curl wget grep sed
      
      wget -q https://github.com/multiarch/qemu-user-static/releases/latest/download/qemu-aarch64-static
      chmod +x qemu-aarch64-static
      cp qemu-aarch64-static /usr/bin/
      
      if [ ! -f /proc/sys/fs/binfmt_misc/register ]; then mount binfmt_misc -t binfmt_misc /proc/sys/fs/binfmt_misc; fi
      
      find /proc/sys/fs/binfmt_misc -type f -name 'qemu-*' -exec sh -c 'echo -1 > {}' \;
      cpu=aarch64
      magic='\x7fELF\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xb7\x00'
      mask='\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff'
      qemu=/usr/bin/qemu-aarch64-static
      flags=F
      echo ":qemu-$cpu:M::$magic:$mask:$qemu:$flags" > /proc/sys/fs/binfmt_misc/register
      
      for f in $(grep -rPzl "Bootstrap: docker\\nFrom: " singularity/); do
        docker_im=$(grep -Pzo "Bootstrap: docker\\nFrom: \K(.*)" "$f" | tr -d "\0")
        sing_im="$(echo "${docker_im}-arm64.sif" | tr ":" "-" | tr "/" "-")"
        echo "Docker image: ${docker_im}"
        echo "Singularity image: ${sing_im}"
        [ -f /root/.singularity/cache/${sing_im} ] || singularity pull --arch arm64 /root/.singularity/cache/${sing_im} docker://${docker_im}
        sed -i -z "s#Bootstrap: docker\nFrom: [^\n]*\n#Bootstrap: localimage\nFrom: /root/.singularity/cache/${sing_im}\n#" "$f"
      done
      
      echo -e "machine gitlab.fel.cvut.cz\nlogin gitlab-ci-token\npassword ${ARO_STUDENT_REPO_ACCESS_TOKEN}" >> /root/.netrc
      PATH=`pwd`/singularity:$PATH /bin/bash singularity/build_images
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file singularity/robolab_noetic.simg ${PACKAGE_REGISTRY_URL}/robolab_noetic_arm64.simg
    - |
      sha256sum singularity/robolab_noetic.simg | cut -d' ' -f1 > singularity/robolab_noetic.simg.sha256sum
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file singularity/robolab_noetic.simg.sha256sum ${PACKAGE_REGISTRY_URL}/robolab_noetic_arm64.simg.sha256sum

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    # TODO: Some time in future, the "filepath" arguments have to be changed to direct_asset_path. However, release-cli
    #       doesn't support that yet (track https://gitlab.com/gitlab-org/release-cli/-/issues/165 ).
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"robolab_noetic_amd64.simg\",\"url\":\"${PACKAGE_REGISTRY_URL}/robolab_noetic_amd64.simg\",\"filepath\":\"/robolab_noetic_amd64.simg\"}" \
        --assets-link "{\"name\":\"robolab_noetic_arm64.simg\",\"url\":\"${PACKAGE_REGISTRY_URL}/robolab_noetic_arm64.simg\",\"filepath\":\"/robolab_noetic_arm64.simg\"}" \
        --assets-link "{\"name\":\"robolab_noetic_amd64.simg.sha256sum\",\"url\":\"${PACKAGE_REGISTRY_URL}/robolab_noetic_amd64.simg.sha256sum\",\"filepath\":\"/robolab_noetic_amd64.simg.sha256sum\"}" \
        --assets-link "{\"name\":\"robolab_noetic_arm64.simg.sha256sum\",\"url\":\"${PACKAGE_REGISTRY_URL}/robolab_noetic_arm64.simg.sha256sum\",\"filepath\":\"/robolab_noetic_arm64.simg.sha256sum\"}"
