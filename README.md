# Singularity Images and Build Scripts for Turtlebots in KN:E-130

## Images for Download

You have to download the correct image for your CPU architecture

 - [amd64/x86-64](https://gitlab.fel.cvut.cz/api/v4/projects/robolab%2Fdeploy/releases/permalink/latest/downloads/robolab_noetic_amd64.simg) (most notebooks and desktops with Intel or AMD CPUs, Intel Macs)
 - [arm64/aarch64](https://gitlab.fel.cvut.cz/api/v4/projects/robolab%2Fdeploy/releases/permalink/latest/downloads/robolab_noetic_arm64.simg) (M1/M2 Macs, Raspberry Pi 64bit, NVidia Jetson, few other notebooks with ARM CPUs)
 - ~~armhf/arm32~~ (32bit Raspberry pi) - unsupported
 
If you choose wrong architecture, you will see an error like this when trying to run the image:

> FATAL:   While checking image: could not open image robolab_noetic_amd64.simg: the image's architecture (amd64) could not run on the host's (arm64)

The script `scripts/download_singularity_image` will download the correct image automatically for you.

## Singularity vs. Apptainer

[Singularity CE](https://sylabs.io/singularity/) == [Apptainer](https://apptainer.org/) (the latter is just a newer name). You can find both terms when googling.

Apptainer is the newer one, but it installs the backwards-compatible `singularity` command to the system.

We use exclusively `singularity` everywhere to make it possible to use both newer and older versions.

## Installing Singularity CE (Apptainer)

On Ubuntu-based systems, Singularity can be installed easily by calling `scripts/install_singularity` . On other systems,
follow [the official install guide](https://apptainer.org/admin-docs/master/installation.html) or you can try
directly installing from the Apptainer releases page: https://github.com/apptainer/apptainer/releases .

> Do not `sudo apt install singularity` !!! This installs a game called Singularity, not the virtualization environment.
> The correct name is either `singularity-ce` or `apptainer`.

### Release Notes of the Latest Release

Release notes of the latest image release can be found here: https://gitlab.fel.cvut.cz/robolab/deploy/-/releases/permalink/latest .
 
## Instructions for Teachers/Maintainers

To create a new release of the Singularity images, just create a tag like `2024.1` and push it to the repo (or you can create it in Gitlab UI).
The build of the images is automated using Gitlab CI and usually takes about 2.5 hours. You can see the running build in [Pipelines](https://gitlab.fel.cvut.cz/robolab/deploy/-/pipelines) .
The amd64 build runs much faster (~25 mins.), so if you see some failure in this build, you can cancel the running arm64 build job, fix the problem and recreate the tag.

### Push a New Release Tag

    tag=2024.rc3; git add . && git commit -m "Release ${tag}." && git tag ${tag} && git push origin --tags master

### Fixup a Release Tag that Failed to Build

Use this command to delete the wrong tag and recreate it on a different commit:

    tag=2024.rc2; git add . && git commit -m "Fixed CI." && git tag --delete ${tag} && git tag ${tag} && git push origin --tags :${tag} && git push origin --tags master