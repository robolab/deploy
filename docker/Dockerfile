FROM ghcr.io/sloretz/ros:noetic-simulators-osrf

SHELL ["/bin/bash", "-c"] 
ADD . /deploy

RUN --mount=type=secret,id=ARO_STUDENT_REPO_ACCESS_TOKEN if [ "$(cat /run/secrets/ARO_STUDENT_REPO_ACCESS_TOKEN)" != "" ]; then \
      echo -e "machine gitlab.fel.cvut.cz\nlogin gitlab-ci-token\npassword $(cat /run/secrets/ARO_STUDENT_REPO_ACCESS_TOKEN)" >> /root/.netrc; \
    fi 

# Brute support layer
RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install -y make cmake gcc file bc joe nano sed gawk git libxt6 libxrandr2 ghostscript php-cli sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    adduser tester --home /home/tester --shell /bin/bash --uid 1003 --gecos "BRUTE support" --disabled-password && \
    adduser student --home /home/student --shell /bin/bash --uid 1004 --gecos "BRUTE student support" --disabled-password && \
    ln -s /local/matlab/bin/matlab /usr/local/bin/matlab && ln -s /local/matlab/bin/matlab /usr/local/bin/matlab8 && \
    ln -s /local/matlab/bin/matlab /usr/local/bin/matlab9 && \
    echo "tester ALL=(student) NOPASSWD: ALL" >> /etc/sudoers && \
    mkdir -p /local/script && mkdir -p /local/data

RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install wget lsb-release git tree -y && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/*
    
RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y && \
    DEBIAN_FRONTEND=noninteractive xargs -a /deploy/config/robolab_noetic.apt apt-get install -y && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/*

RUN rosdep update && apt-get update -y && \
    cd /deploy && ws_parent=/opt/ros rosdep=y scripts/setup_noetic_workspaces && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/* && (rm -rf /root/.netrc /tmp/* || true)
