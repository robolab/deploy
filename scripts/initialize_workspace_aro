#!/bin/bash -e

# get the path to this script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
REPO_PATH="$( cd "$SCRIPT_DIR/.." && pwd )"
ROOT_PATH="$( cd "$REPO_PATH/.." && pwd )"
WORKSPACE_PATH="$ROOT_PATH/workspace"
STUDENT_PACKAGES_PATH="$WORKSPACE_PATH/src/student-packages"

. "$SCRIPT_DIR/utils.sh"

if [ "$SINGULARITY_NAME" != "robolab_noetic.simg" ]; then
	echo "You are not inside the ARO singularity container."
	echo "Please start the singularity first using start_singularity_aro."
	exit 1
fi

echo "Creating workspace if it does not exist in $WORKSPACE_PATH ."
if [ -d "$STUDENT_PACKAGES_PATH" ]; then
	if ! is_online; then
		echo "You do not seem to be online. Not updating student packages repo."
	else
		echo "Pulling new changes into the student repo in $STUDENT_PACKAGES_PATH ."
		cd "$STUDENT_PACKAGES_PATH"
		git fetch origin
		if ! git_merge_autostash origin/master; then
			echo "Pulling teacher changes into student repo failed. You need to resolve the merge conflicts yourself to proceed."
			echo "Try running the following commands:"
			echo "cd \"$STUDENT_PACKAGES_PATH\" && git add . && git commit -m \"My changes\" && git merge origin/master"
			echo "If that fails and you are unable to resolve the merge conflict, please, contact the lab teachers."
			exit 1
		fi
	fi
else
	mkdir -p "$STUDENT_PACKAGES_PATH/.."
	echo "Cloning the student repo into $STUDENT_PACKAGES_PATH . All your work on ARO will be happening there."
	git clone https://gitlab.fel.cvut.cz/robolab/aro/student-packages "$STUDENT_PACKAGES_PATH" 
fi

cd "$WORKSPACE_PATH"

if [ ! -d ".catkin_tools" ]; then
	echo "Initializing your ARO ROS workspace."
	catkin init
	catkin config --extend /opt/ros/aro
fi

if [ ! -d build ] || [ ! -d devel ]; then
	echo "Building the ARO ROS workspace."
	catkin build
fi

# Configure tmux to start with bash by default if there is no user configuration
if [ ! -f "$HOME"/.tmux.conf ]; then
	echo "set-option -g default-shell /bin/bash" > "$HOME"/.tmux.conf
fi

# Create a Python wrapper that can be used as interpreter in IDEs. This interpreter correctly resolves the workspace.
if [ ! -f "${WORKSPACE_PATH}/python" ]; then
	cat << "EOF" > "${WORKSPACE_PATH}/python"
#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

args=("$@")
while [[ $# -gt 0 ]]; do shift; done

. $DIR/devel/setup.bash
PYTHONPATH="$DIR/src/student-packages/aro_exploration/src:$PYTHONPATH" python3 "${args[@]}"
EOF
  chmod +x "${WORKSPACE_PATH}/python"
fi

cd "$STUDENT_PACKAGES_PATH"
echo "Starting interactive bash while sourcing the workspace."
if [ $# -gt 0 ]; then
	exec bash -c "source \"$WORKSPACE_PATH/devel/setup.bash\"; $*"
else
	exec bash --init-file <(echo "source \"$WORKSPACE_PATH/devel/setup.bash\"")
fi