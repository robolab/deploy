function is_online() {
	local url="https://www.cvut.cz/sites/default/files/favicon.ico"
	local domain="cvut.cz"
	if which wget >/dev/null; then
		timeout 1 wget --spider -q -O /dev/null "$url" && return 0 || return 1
	elif which curl >/dev/null; then
		timeout 1 curl -sfI -o /dev/null "$url" && echo 1 || echo 0
	elif which ping; then
		timeout 1 ping -c1 "$domain" 2>/dev/null 1>/dev/null
		[ "$?" = "0" ] && return 1
		[ "$?" = "126" ] && return 1  # Operation not permitted; this happens inside Singularity. In that case, succeed.
		return 0
	else
		return 0
	fi
}

# Git has merge --autostash since 2.27, but Focal has only 2.25. Moreover, autostash from 2.27 is buggy.
function git_merge_autostash() {
	remote_branch="$1"
	
	local_branch="$(git rev-parse --abbrev-ref HEAD)"
	local_commit="$(git rev-list --max-count=1 "$local_branch")"
	remote_commit="$(git rev-list --max-count=1 "$remote_branch")"
	num_missing_commits="$(git rev-list --count "$local_commit".."$remote_commit")"
	if [ $num_missing_commits -eq 0 ]; then
		echo "Local repository is up-to-date."
		return 0
	fi
	
	echo "Merging upstream changes."
	echo "Local commit: $local_commit"
	echo "Remote commit: $remote_commit"

	backup="/tmp/student-packages-$(date +%s).backup.tar.gz"
	tar -czf "$backup" .

	stashes_before="$(git stash list)"
	git stash
	stashes_after="$(git stash list)"
	stash_created=0
	[ "$stashes_before" != "$stashes_after" ] && stash_created=1
	[ "$stash_created" = "1" ] && echo "Local changes were stashed before the update. If you cannot find them in case of failure, run 'git stash list' and 'git stash pop'. As a last resort, the repository was backed up to $backup."

	if ! git merge "$remote_branch"; then
		err=$?
		git merge --abort || true
		[ "$stash_created" = "1" ] && git stash pop
		return $err
	else
		[ "$stash_created" = "1" ] && git stash pop
		return 0
	fi
}